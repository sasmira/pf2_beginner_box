# Patchnotes
## Compatibility
- Ready for v11 version.


## Update du 20/11/2023
- Mise à jour du module Menace sous Otari v5.93
    - Remise en forme du module.
    - Ajout de journal par niveau.
    - Corrections mineures.

## Update du 16/11/2023
- Mise à jour du module Menace sous Otari v5.90
    - Correction des monstres défectueux
    - Amélioration de la visibilité des tokens
    - Amélioration des sons et ambiances

## v1.0.0
- Release version
